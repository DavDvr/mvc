<?php

use app\App;
use app\Controller\PostsController;


define('ROOT', dirname(__DIR__));

require ROOT . '/app/App.php';
App::load();

if (isset($_GET['p'])){

    $page = $_GET['p'];

}else {
    $page = 'posts.index';
}

//je recupère la page, je lui enleve le point donc cela donnera:   posts index
$page = explode('.', $page);

// je recupère l'action donc index
$action = $page[1];

//je dis si la page est admin, pour reupérer l'action de:  admin.posts.add
if ($page[0] == 'admin'){

    //je vais dans le bon namespace Admin et je récupère le controller
    $controller = '\app\Controller\Admin\\'. ucfirst($page[1]) . 'Controller';

    //je recupère l'action add dans l'exemple que j'ai donné plus haut
    $action = $page[2];

}else {

    //sinon je vais dans le namespace classique et recupère le controller
    $controller = '\app\Controller\\'. ucfirst($page[0]) . 'Controller';
}

$controller = new $controller();
$controller->$action();
