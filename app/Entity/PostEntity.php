<?php


namespace app\Entity;

use core\Entity\Entity;

class PostEntity extends Entity
{
    protected $table = 'articles';

    public function getUrl():string{
        return 'index.php?p=posts.show&id=' . $this->id;
    }

    public function getExtrait(){
        $html =  '<p>'. substr($this->contenu, 0, 100) . '...</p>';
        $html .= '<p> <a href="'. $this->getURL() .'">Voir la suite</a></p>';
        return $html;
    }

}