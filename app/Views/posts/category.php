
<h1><?= $categorie->titre;?></h1>

<div class="row">
    <div class="col-sm-8">
        <ul>
            <?php foreach ($articles as $post):?>

                <h2><a href="<?=$post->url;?>"><?= $post->titre;?></a></h2>
                <p><em><?= $post->categories;?></em></p>
                <p><?= $post->extrait;?></p>
            <?php endforeach;?>
        </ul>
    </div>
    <div class="col-sm-4">
        <ul>
            <?php foreach ($categories as $cat):?>
                <li><a href="<?= $cat->url;?>"><?= $cat->titre?></a></li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
