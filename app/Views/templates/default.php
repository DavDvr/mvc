<?php

use App\App;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= App::getInstance()->title; ?></title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<nav class="navbar fixed-top navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">MVC POO</a>
    </div>
</nav>

<div class="container">
    <div class="starter-template" style="padding-top: 100px">
      <?= $content; ?>
    </div>
</div>


</body>
</html>


