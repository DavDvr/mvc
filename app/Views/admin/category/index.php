<h1>Administrer les categories</h1>

<p>
    <a class="btn btn-success" href="?p=admin.category.add">Ajouter</a>
</p>

<table class="table">
    <thead>
        <tr>
            <td>ID</td>
            <td>Titre</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($categories as $cat): ?>
        <tr>
            <td><?= $cat->id; ?></td>
            <td><?= $cat->titre; ?></td>
            <td>
                <a href="?p=admin.category.edit&id=<?= $cat->id; ?>" class="btn btn-primary">Edit</a>

                <form action="?p=admin.category.delete" method="post" style="display: inline">
                    <input type="hidden" name="id" value="<?= $cat->id; ?>">
                    <button type="submit"  class="btn btn-danger">Delete</button>
                </form>

            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>