<form method="post" action="">
    <?= $form->input('titre', "Titre de l'article")?>
    <?= $form->input('contenu', 'Contenu', ['type' => 'textarea'])?>
    <?= $form->select('categories_id', 'Category', $categories)?>
    <?= $form->submit("btn btn-primary form-control mt-2", 'Save')?>
</form>
