<h1>ici mon admin</h1>

<p>
    <a class="btn btn-success" href="?p=admin.posts.add">Ajouter</a>
</p>

<table class="table">
    <thead>
        <tr>
            <td>ID</td>
            <td>Titre</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post->id; ?></td>
            <td><?= $post->titre; ?></td>
            <td>
                <a href="?p=admin.posts.edit&id=<?= $post->id; ?>" class="btn btn-primary">Edit</a>

                <form action="?p=admin.posts.delete" method="post" style="display: inline">
                    <input type="hidden" name="id" value="<?= $post->id; ?>">
                    <button type="submit"  class="btn btn-danger" href="?p=admin.posts.delete&id=<?= $post->id; ?>">Delete</button>
                </form>

            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>