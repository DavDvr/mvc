<?php if ($errors):?>
<div class="alert alert-danger">
    Identifiants incorrects
</div>
<?php endif; ?>
<form method="post" action="">
    <?= $form->input('username', 'Pseudo')?>
    <?= $form->input('password', 'Mot de passe', ['type' => 'password'])?>
    <?= $form->submit("btn btn-primary form-control mt-2", 'Send')?>
</form>
