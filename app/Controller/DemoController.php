<?php

namespace app\Controller;

use core\Database\QueryBuilder;

class DemoController
{
    public function index(){

    require ROOT . '/Query.php';

       echo \Query::select('id', 'titre', 'contenu')
           ->from('articles', 'Post')
           ->where('Post.categories_id = 2')
           ->where('Post.date > NOW()');
    }
}
