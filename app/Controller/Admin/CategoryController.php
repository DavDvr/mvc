<?php


namespace app\Controller\Admin;


use core\HTML\BootstrapForm;

class CategoryController extends \app\Controller\Admin\AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel('Category');
    }

    public function index(){
        $categories = $this->Category->all();
        $this->render('admin.category.index', compact('categories'));
    }

    public function add(){

        if (!empty($_POST)){

            $result = $this->Category->create(
                [
                    'titre' => $_POST['titre'],
                ]
            );
            if($result){

               header("Location: ?p=admin.category.index");
            }
        }

        $form = new BootstrapForm($_POST);

        $this->render('admin.category.edit', compact('form'));
    }

    public function delete(){

        if (!empty($_POST)){
            $this->Category->delete($_POST['id']);
            $this->index();
        }
    }
    public function edit(){

        if (!empty($_POST)){

             $this->Category->update($_GET['id'],
                [
                    'titre' => $_POST['titre'],
                ]
            );
                header("Location: ?p=admin.category.index");
        }

        $cat = $this->Category->find($_GET['id']);
        $form = new BootstrapForm($cat);
        $this->render('admin.category.edit', compact('form'));
    }

}