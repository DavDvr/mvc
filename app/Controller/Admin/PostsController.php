<?php


namespace app\Controller\Admin;


use core\HTML\BootstrapForm;

class PostsController extends \app\Controller\Admin\AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel('Post');
        $this->loadModel('Category');
    }

    /**
     * Accueil administration des posts
     */
    public function index(){
        $posts = $this->Post->all();
        $this->render('admin.posts.index', compact('posts'));
    }

    /**
     * Ajout d'un post
     */
    public function add(){

        if (!empty($_POST)){

            $result = $this->Post->create(
                [
                    'titre' => $_POST['titre'],
                    'contenu' => $_POST['contenu'],
                    'categories_id' => $_POST['categories_id'],
                ]
            );

            if($result){

               return $this->index();

            }
        }

        $categories = $this->Category->extract('id', 'titre');

        $form = new BootstrapForm($_POST);

        $this->render('admin.posts.edit', compact('categories', 'form'));
    }

    /**
     * Suppression des posts
     */
    public function delete(){

        if (!empty($_POST)){
            $this->Post->delete($_POST['id']);
            return $this->index();
        }
    }

    /**
     * Modification des posts
     */
    public function edit(){

        if (!empty($_POST)){

            $result = $this->Post->update($_GET['id'],
                [
                    'titre' => $_POST['titre'],
                    'contenu' => $_POST['contenu'],
                    'categories_id' => $_POST['categories_id'],
                ]
            );
            if($result){
               return $this->index();
            }
        }

        $post = $this->Post->find($_GET['id']);
        $categories = $this->Category->extract('id', 'titre');

        $form = new BootstrapForm($post);
        $this->render('admin.posts.edit', compact('categories', 'form'));
    }
}
