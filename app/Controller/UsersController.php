<?php


namespace app\Controller;


use app\App;
use core\Auth\DBAuth;
use core\HTML\BootstrapForm;

class UsersController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function login(){
        $errors = false;
        if (!empty($_POST)){

            $auth = new DBAuth(App::getInstance()->getDb());

            if($auth->login($_POST['username'], $_POST['password'])){

                header("Location:index.php?p=admin.posts.index");

            }else{
                $errors = true;
            }
        }

        $form = new BootstrapForm($_POST);
        $this->render('users.login', compact('form', 'errors'));
    }

}