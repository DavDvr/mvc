<?php

namespace app\Controller;

class PostsController extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel('Post');
        $this->loadModel('Category');
    }

    public function index(){
        $posts = $this->Post->last();
        $categories = $this->Category->allCat();
        $this->render('posts.index',compact('posts', 'categories'));

    }

    public function category(){

        $categorie = $this->Category->find($_GET['id']);

        if ($categorie === false) {
            $this->notFound();
        }

        $articles = $this->Post->lastByCat($_GET['id']);
        $categories = $this->Category->allCat();
        $this->render('posts.category',compact('articles', 'categories', 'categorie'));
    }

    public function show(){

        $article = $this->Post->findWithCategory($_GET['id']);
        if ($article === false) {
            $this->notFound();
        }
        $this->render('posts.show',compact('article'));
    }
}

