<?php

namespace app\Controller;


use app\App;
use core\Controller\Controller;
use core\HTML\BootstrapForm;

class AppController extends Controller
{
    protected $template = 'default';

   public function __construct()
   {
       $this->viewPath = ROOT . '/app/Views/';

   }

   protected function loadModel($model){
       $this->$model = App::getInstance()->getTable($model);
   }

    protected function lastInsertId()
   {
       return App::getInstance()->getDb()->lastInsertId();
   }
}
