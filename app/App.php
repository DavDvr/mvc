<?php


namespace app;

use core\Config;
use core\Database\MysqlDatabase;
use core\Autoloader;

class App
{

    public $title = "Mon super site";
    private static $_instance;
    private  $db_instance;
    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)){

            self::$_instance = new App();
        }

        return self::$_instance;
    }

    public static function load(){
        session_start();
        require ROOT . '/app/Autoloader.php';
        \app\Autoloader::register();

        require ROOT . '/core/Autoloader.php';
        Autoloader::register();
    }

    public function getTable($name){
        $class_name = 'app\\Table\\' . ucfirst($name) . 'Table';
        return new $class_name($this->getDb());
    }

    public function getDb()
    {
        if (is_null($this->db_instance)){
            $conf = new Config(ROOT . '/config/config.php');
            $this->db_instance = new MysqlDatabase($conf->get("db_name"), $conf->get("db_user"), $conf->get("db_pass"), $conf->get("db_host"));

        }

        return $this->db_instance ;
    }
}
