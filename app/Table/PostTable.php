<?php


namespace app\Table;

use App\Entity\PostEntity;
use core\Table\Table;

class PostTable extends Table
{
    protected $table = 'articles';
    /**
     * recupère les derniers artciles
     * @return array
     */
    public function last():array
    {
        return $this->query("select articles.id, articles.titre, articles.contenu, categories.titre as categories 
        from articles 
        left join categories on categories_id = categories.id
        order by articles.date DESC");
    }

    /**
     * @return PostEntity
     * @param int $id
     * recupère un article en liant la categorie associée
     */
    public function findWithCategory($id)
    {
        return $this->query("select articles.id, articles.titre, articles.contenu, articles.date, categories.titre as categories 
        from articles 
        left join categories on categories_id = categories.id
        where articles.id = ?",
            [$id], true
        );
    }

    /**
     * @param $category_id
     * @return array
     */
    public function lastByCat($category_id):array {
        return $this->query("
        select articles.id, articles.titre, articles.contenu, categories.titre as categories 
        from articles
        left join categories on categories_id = categories.id
        where articles.categories_id = ?
        order by articles.date DESC         
        ",[$category_id]);
    }

    public function all()
    {
        return $this->query("select * from articles");
    }

}