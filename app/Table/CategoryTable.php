<?php


namespace app\Table;

use core\Table\Table;

class CategoryTable extends Table
{
    protected $table = 'categories';

    public function allCat()
    {
        return $this->query("select * from categories");
    }
}