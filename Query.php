<?php

/**
 * Class Query
 * Facade permet de cacher une partie du code
 */
class Query
{
    public static function __callStatic($method, $arguments)
    {
        $query = new \core\Database\QueryBuilder();
       return call_user_func_array([$query, $method], $arguments);
    }
}