<?php

namespace core;
/**
 * Class Autoloader
 * @package App
 */
class Autoloader
{

    /**
     * enregistre l'autoloader
     */
    static function register(){
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    /**
     * inclut le fichier correspondant à ma class
     * @param $class
     */
    static function autoload($class){
        if (strpos($class,__NAMESPACE__. '\\') === 0){
            $class = str_replace(__NAMESPACE__. '\\', '', $class);
            $class = str_replace('\\','/',$class);
            require __DIR__. '/' .$class. '.php';
        }
    }
}