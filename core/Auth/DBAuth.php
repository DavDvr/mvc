<?php

namespace core\Auth;

use core\Database\Database;

class DBAuth
{
    private Database $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @return false|mixed
     */
    public function getUserId(){

        if ($this->logged()){
            return $_SESSION['auth'];
        }
        return false;
    }

    /**
     * @param $username
     * @param $password
     * @return boolean
     */
    public function login($username, $password)
    {
        $user = $this->db->prepare("select * from users where username = ?",[$username], null, true);

        if ($user){
           if ($user->password === sha1($password)){
              $_SESSION['auth'] = $user->id;
              return true;
           }
        }
    }

    public function logged()
    {
        return isset($_SESSION['auth']);
    }
}