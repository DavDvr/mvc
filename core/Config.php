<?php


namespace core;


class Config
{

    private $settings=[];
    private static $_instance;

    public function __construct($file_config)
    {
        $this->settings = require($file_config);
    }

    /**
     * return l'instance de la la class Config
     * @return Config
     */
    public static function getInstance($file_config){

        if(is_null(self::$_instance)){
            self::$_instance = new Config($file_config);
        }
        return self::$_instance;
    }

    /**
     * permet d'obtenir les db_name, db_user, db_pass ,db_host pour la connexion à la bdd
     * @param $key
     * @return mixed|null
     */
    public function get($key){
        if(!isset($this->settings[$key])){
            return null;
        }else{
            return $this->settings[$key];
        }
    }
}