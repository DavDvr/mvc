<?php

namespace core\Table;

use core\Database\Database;

class Table

{
    protected $table;
    private Database $db;


    public function __construct(Database $db)
    {
        $this->db = $db;
        if (is_null($this->table)){
           $parts = explode('\\',get_class($this));
           $class_name = end($parts);
           $this->table = strtolower(str_replace('Table', '', $class_name)) . 's';
        }
    }


    public function query($statement, $attributes = null, $one = false)
    {
        if ($attributes){
            return $this->db->prepare(
                $statement,
                $attributes,
                str_replace('Table', 'Entity', get_class($this)),
                $one
            );
        }else {
            return $this->db->query(
                $statement,
                str_replace('Table', 'Entity', get_class($this)),
                $one
            );
        }

    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->query("select * from {$this->table}");
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    public function extract($key, $value){

        $records = $this->all();

        $return = [];

        foreach ($records as $v){
            $return[$v->$key] = $v->$value;
        }

        return $return;
    }

    public function find($id){
        return $this->query("select * from {$this->table} where id = ?
        ", [$id], true
        );
    }


    /**
     * @param $id
     * @param $fields
     * @return mixed
     */
    public function update($id, $fields){
        $sql_parts = [];
        $attributes = [];

        foreach ($fields as $nameField => $value){
            $sql_parts[] = " $nameField = ?";
            $attributes[] = $value;
        }
        $attributes[] = $id;
        $sql_part = implode(', ', $sql_parts);

        return $this->query("update {$this->table} set {$sql_part} where id= ?",
            $attributes, true);
    }

    /**
     * @param $fields
     * @return mixed
     */
    public function create($fields){
        $sql_parts = [];
        $attributes = [];

        foreach ($fields as $nameField => $value){
            $sql_parts[] = " $nameField = ?";
            $attributes[] = $value;
        }

        $sql_part = implode(', ', $sql_parts);

        return $this->query("insert into {$this->table} set {$sql_part}",
            $attributes, true);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id){
        return $this->query("delete from {$this->table} where id = ?", [$id], true);
    }
}
