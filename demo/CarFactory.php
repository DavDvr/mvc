<?php

namespace demo;

class CarFactory
{
    public static function getCar($type){
        $type = ucfirst($type);
        $class_name = "get$type";
        return new $class_name;
    }
}